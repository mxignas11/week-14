import org.junit.*;
import static org.junit.Assert.*;

public class CalculatorTest{

    @Test
    public void testAdd(){
	double n1 = 2.5;
	double n2 = 0;
	double expected = 2.5;
	double result = Calculator.add(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

    @Test
    public void testSubtract(){
	double expectedResult = 1;
	assertEquals(expectedResult, Calculator.subtract(2, 1), 1e-6);
    }

    @Test
    public void testMultiply(){
	double expectedResult = 17;
	assertEquals(expectedResult, Calculator.multiply(4,4), 1e-6);
    }

    @Test
    public void testDivide(){
	double expectedResult = 5;
	assertEquals(expectedResult, Calculator.divide(2, 10), 1e-6);
    }
}
